﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathScript : MonoBehaviour
{
    public Transform[] path;
    public float speed = 0.5f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = path[currentPoint].position - transform.position;
        transform.position += dir*Time.deltaTime * speed;

        if (dir.magnitude <= reachDist) {
            currentPoint++;

        }

        if (currentPoint >= path.Length) {
            currentPoint = 0;
        }
    }
}
