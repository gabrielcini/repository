﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class PlayerControls : MonoBehaviour
{
    Vector3 pos = new Vector3(-6.95f, -2.35f, -1f);
    Vector3 pos2 = new Vector3(-7.924f, -4.218f, -1f);
    Animator anim;
    Animator doorAnim;
    GameObject door;
    int liveCount = 3;
    GameObject heartOne;
    GameObject heartTwo;
    GameObject heartThree;
    int levelNum;
    public static int score;
    string sceneName;
    Scene currentScene;
    public GameObject key;
    bool unlockDoor = false;
    bool disableControls;

    // Start is called before the first frame update
    void Start()
    {
        disableControls = false;
        key.SetActive(true);
        heartOne = GameObject.Find("Heart1");
        heartTwo = GameObject.Find("Heart2");
        heartThree = GameObject.Find("Heart3");
        anim = gameObject.GetComponent<Animator>();
        door = GameObject.Find("Door");
        doorAnim = door.GetComponent<Animator>();
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        PlayerPrefs.SetInt("tryNum", PlayerPrefs.GetInt("tryNum") + 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (liveCount == 2) {
            heartThree.SetActive(false);
        }

        if (liveCount == 1)
        {
            heartTwo.SetActive(false);
        }

        if (liveCount == 0)
        {
            heartOne.SetActive(false);
            CreateText();
            SceneManager.LoadScene("Menu");
        }

        if (disableControls == false)
        { 
            if (Input.GetKey("up"))
            {
                transform.Translate(Vector2.up * 2f * Time.deltaTime, Space.World);
            }

            if (Input.GetKey("down"))
            {
                transform.Translate(Vector2.down * 2f * Time.deltaTime, Space.World);
            }

            if (Input.GetKey("right"))
            {
                transform.Translate(Vector2.right * 2f * Time.deltaTime, Space.World);
            }

            if (Input.GetKey("left"))
            {
                transform.Translate(Vector2.left * 2f * Time.deltaTime, Space.World);
            }

            if (Input.GetKeyDown("up"))
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                anim.Play("SpriteWalk");
            }

            if (Input.GetKeyUp("up"))
            {
                anim.Play("Idle");
            }

            if (Input.GetKeyDown("down"))
            {
                transform.localRotation = Quaternion.Euler(0, 0, 180);
                anim.Play("SpriteWalk");
            }

            if (Input.GetKeyUp("down"))
            {
                anim.Play("Idle");
            }

            if (Input.GetKeyDown("right"))
            {
                transform.localRotation = Quaternion.Euler(0, 0, -90);
                anim.Play("SpriteWalk");
            }

            if (Input.GetKeyUp("right"))
            {
                anim.Play("Idle");
            }

            if (Input.GetKeyDown("left"))
            {
                transform.localRotation = Quaternion.Euler(0, 0, 90);
                anim.Play("SpriteWalk");
            }

            if (Input.GetKeyUp("left"))
            {
                anim.Play("Idle");
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {

        if (collision2D.gameObject.tag == "spike") {
            this.transform.position = pos2;
            this.transform.Rotate(0f, 0f, 0f);
            liveCount--;
            StartCoroutine(Disable());
        }

        if (collision2D.gameObject.tag == "wall" && sceneName == "Level1")
        {
            this.transform.position = pos;
            this.transform.Rotate(0f, 0f, 0f);
            liveCount--;
            StartCoroutine(Disable());
        }

        if (collision2D.gameObject.tag == "wall" && sceneName == "Level2" || collision2D.gameObject.tag == "wall" && sceneName == "Level3")
        {
            this.transform.position = pos2;
            this.transform.Rotate(0f, 0f, 0f);
            liveCount--;
            StartCoroutine(Disable());
        }

        if (collision2D.gameObject.tag == "Enemy" && sceneName == "Level1") {
            this.transform.position = pos;
            this.transform.Rotate(0f, 0f, 0f);
            liveCount--;
            StartCoroutine(Disable());
        }

        if (collision2D.gameObject.tag == "Enemy" && sceneName == "Level2" || collision2D.gameObject.tag == "Enemy" && sceneName == "Level3")
        {
            this.transform.position = pos2;
            this.transform.Rotate(0f, 0f, 0f);
            liveCount--;
            StartCoroutine(Disable());
        }

        if (collision2D.gameObject.name == "Key")
        {
            doorAnim.Play("DoorOpenAnimation");
            key.SetActive(false);
            unlockDoor = true;
        }

        if (collision2D.gameObject.name == "Key2")
        {
            collision2D.gameObject.SetActive(false);
            doorAnim.Play("DoorOpenAnimation2");
            unlockDoor = true;
        }

        if (collision2D.gameObject.name == "Door" && unlockDoor == true) {
            SceneManager.LoadScene("Level2");
        }

        if (collision2D.gameObject.name == "Door" && unlockDoor == true && sceneName == "Level2")
        {
            SceneManager.LoadScene("Level3");
        }

        if (collision2D.gameObject.name == "Door" && unlockDoor == true && sceneName == "Level3")
        {
            SceneManager.LoadScene("Menu");
        }

        if (collision2D.gameObject.tag == "GreenGem") {
            score++;
            Debug.Log(score);
            collision2D.gameObject.SetActive(false);
            
            //System.IO.File.WriteAllText(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\Game Backup\\DungeonMaster\\Dungeon Master\\ScoreData.txt", "Score:          " + score.ToString() + " points");
        }

        if (collision2D.gameObject.tag == "BlueGem")
        {
            score+=10;
            Debug.Log(score);
            collision2D.gameObject.SetActive(false);
            //System.IO.File.WriteAllText(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\Game Backup\\DungeonMaster\\Dungeon Master\\ScoreData.txt", "Score:          " + score.ToString() + " points");
        }
    }

    void CreateText() {
        string path = Application.dataPath + "/ScoreLog.txt";
        if (!File.Exists(path)) {
            File.WriteAllText(path, "\n\n");
        }

        string content = score + " points" + "\r\n";
        File.AppendAllText(path, PlayerPrefs.GetInt("tryNum") + " try  " + content);
    }

    IEnumerator Disable() {
        disableControls = true;
        yield return new WaitForSeconds(1);
        disableControls = false;
    }
}