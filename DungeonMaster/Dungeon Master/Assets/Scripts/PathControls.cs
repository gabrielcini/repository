﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PathControls : MonoBehaviour
{
    Transform target;
    private int num = 0;

    public GameObject playerOb;
    public GameObject walls;
    public GameObject enemy;
    public GameObject key;
    public GameObject door;
    public GameObject greenGems;
    public GameObject blueGems;

    public Physics phy;
    private List<GameObject> GOs;

    Vector3 pos = new Vector3(-7.73f, -4.37f, 0.0f);
    Collider2D pathOneCollider;
    Collider2D pathTwoCollider;
    Collider2D pathThreeCollider;
    Collider2D pathFourCollider;
    Collider2D pathFiveCollider;
    Collider2D pathSixCollider;
    Collider2D pathSevenCollider;
    Collider2D pathEightCollider;
    Collider2D pathNineCollider;
    Collider2D pathTenCollider;

    private float baseAngle;
    private float newAngle;
    private bool doRotation;
    private float rotationEndTime;

    bool isCreated = true;
    bool spawned = false;
    bool spawnPlayer;
    bool canControl = true;

    string sceneName;
    Scene currentScene;

    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        enemy.SetActive(false);
        playerOb.SetActive(false);
        walls.SetActive(false);
        //enemy.SetActive(false);
        key.SetActive(false);
        door.SetActive(false);
        greenGems.SetActive(false);
        blueGems.SetActive(false);
        canControl = true;
        isCreated = true;
        spawnPlayer = false;
        pathOneCollider = GameObject.Find("Path1").GetComponent<Collider2D>();
        pathTwoCollider = GameObject.Find("Path2").GetComponent<Collider2D>();
        pathThreeCollider = GameObject.Find("Path3").GetComponent<Collider2D>();
        pathFourCollider = GameObject.Find("Path4").GetComponent<Collider2D>();
        pathFiveCollider = GameObject.Find("Path5").GetComponent<Collider2D>();
        pathSixCollider = GameObject.Find("Path6").GetComponent<Collider2D>();
        pathSevenCollider = GameObject.Find("Path7").GetComponent<Collider2D>();
        pathEightCollider = GameObject.Find("Path8").GetComponent<Collider2D>();
        pathNineCollider = GameObject.Find("Path9").GetComponent<Collider2D>();
        pathTenCollider = GameObject.Find("Path10").GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Rotate(Vector3 axis, float angle, float duration = 1.0f)
    {
        Quaternion from = transform.rotation;
        Quaternion to = transform.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            transform.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.rotation = to;
    }

    void OnMouseDown() {
        if (canControl == true) {
            StartCoroutine(Rotate(Vector3.forward, 90, 0.2f));
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (pathOneCollider.IsTouching(pathTwoCollider) && pathTwoCollider.IsTouching(pathOneCollider)
            && pathTwoCollider.IsTouching(pathThreeCollider)
            && pathThreeCollider.IsTouching(pathTwoCollider) && pathThreeCollider.IsTouching(pathFourCollider)
            && pathFourCollider.IsTouching(pathFiveCollider) && pathFiveCollider.IsTouching(pathSixCollider)
            && pathSixCollider.IsTouching(pathSevenCollider) && pathSevenCollider.IsTouching(pathEightCollider)
            ||
            pathOneCollider.IsTouching(pathTwoCollider) && pathTwoCollider.IsTouching(pathOneCollider)
            && pathTwoCollider.IsTouching(pathThreeCollider)
            && pathThreeCollider.IsTouching(pathTwoCollider) && pathThreeCollider.IsTouching(pathFourCollider)
            && pathFourCollider.IsTouching(pathFiveCollider) && pathFiveCollider.IsTouching(pathSixCollider)
            && pathSixCollider.IsTouching(pathSevenCollider) && pathSevenCollider.IsTouching(pathEightCollider) 
            && pathEightCollider.IsTouching(pathNineCollider)
            && pathNineCollider.IsTouching(pathTenCollider))
        {
            canControl = false;
            playerOb.SetActive(true);
            walls.SetActive(true);
            enemy.SetActive(true);
            //key.SetActive(true);
            door.SetActive(true);
            greenGems.SetActive(true);
            blueGems.SetActive(true);
        }
    }
}